
class Agility {


    //bigger than

    //Show if the first number is bigger than the second

    fun biggerThan(numA: String, numB:String):Boolean
    {
        val result1: Boolean
        val number1:Double = numA.toDouble()
        val number2:Double = numB.toDouble()
        result1 = number1>number2
        return result1
    }


    //Sort from bigger the numbers an show in list

    fun order(numA:Int, numB:Int, numC:Int, numD:Int, numE:Int): List<Int?>
    {
        var myOrderedList=listOf(numA,numB,numC,numD,numE)
        myOrderedList= myOrderedList.sorted()
        return myOrderedList
    }

    //Look for the smaller number of the list

    fun smallerThan(list: List<Double>): Double{
        var x:Double=list[0]
        for(i in list)
            if (i < x)
                x = i
        return x
    }
//Palindrome number is called in Spanish capicúa
    //The number is palindrome

    fun palindromeNumber(numA: Int): Boolean
    {
        val list= mutableListOf<Int>()
        var palindrome = 0
        var i = numA
        while (i/10>0) {
            list += i % 10
            i /= 10

        }
        list += i % 10
        //println(list)
        list.forEach {
            palindrome += it
            palindrome *= 10
        }
        palindrome/=10
        return palindrome==numA
    }
    //the word is palindrome?
    fun palindromeWord(word: String): Boolean
    {
        val x = StringBuilder(word)
        val palindrome2 = x.reverse().toString()
        return word == palindrome2
    }

    //Show the factorial number for the parameter
    fun factorial(numA: Int):Int
    {
        var x=1
        var fact=numA
        while (fact>0) {
            x *= fact
            fact -= 1
        }
        return x
    }

    //is the number odd
    fun is_Odd(numA: Byte): Boolean {
        var number = numA.toInt()
        return number % 2 != 0
    }

    //is the number prime
    fun isPrimeNumber(numA:Int): Boolean
    {
        var i = 2
        var number = numA
        var prime:Boolean=true

        if (number<0) {
            prime = false
        }else if (number != 2 && number!=3) {
            while (i <= number / 2) {
                //println(i)
                if (number % i == 0) {
                    prime = false
                    break
                }
                i++
            }
        }
        return prime
    }

    //is the number even

    fun is_Even(numA: Byte): Boolean
    {
        var number = numA.toInt()
        return number % 2 == 0
    }
    //is the number perfect
    fun isPerfectNumber(numA:Int): Boolean
    {
        var Number:Int
        var y:Int = 0
        var i:Int = 1
        while(i<numA){
            Number= numA % i
            if (Number == 0){
                y+=i }
            i++
        }
        return y == numA
    }
    //Return an array with the fibonacci sequence for the requested number
    fun fibonacci(numA: Int): List<Int?>
    {
        var x=0
        var y=1
        var suma:Int
        val fibonacci= mutableListOf<Int?>()
        for (i in 0..numA){
            fibonacci.add(x)
            suma = x + y
            x = y
            y = suma
        }
        return fibonacci
    }
    //how many times the number is divided by 3
    fun timesDividedByThree(numA: Int):Int
    {
        return numA/3
    }

    //The game of fizzbuzz
    fun fizzBuzz(numA: Int):String?
    {
        var fizz:String
        if (numA%3 == 0 && numA%5==0) {
            fizz = "FizzBuzz"
        }else if (numA%3 == 0) {
            fizz = "Fizz"
        }else if (numA%5 == 0) {
            fizz = "Buzz"
        }else{
            fizz=numA.toString()
        }
        return fizz
    }

}