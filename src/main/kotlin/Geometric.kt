import java.lang.Math.PI
import kotlin.math.hypot
import kotlin.IllegalArgumentException as KotlinIllegalArgumentException1
import kotlin.IllegalArgumentException as KotlinIllegalArgumentException

/**
 * @author sigmotoa
 */

class Geometric {
    //Calculate the area for a square using one side
    fun squareArea(side: Int):Int{

        if(side>=0) {
            return side * side
        }else{
            throw IllegalArgumentException()
        }
    }

    //Calculate the area for a square using two sides
    fun squareArea(sideA: Int, sideB: Int): Int{
        if(sideA>=0 ) {
            if(sideB>=0){
            return sideA*sideB
            }else{
                throw java.lang.IllegalArgumentException()
            }
        }else{
            throw java.lang.IllegalArgumentException()
        }
    }

    //Calculate the area of circle with the radius
    fun circleArea(radius: Double): Double {
        if(radius>=0){
            return PI*radius*radius
        }else{
            throw java.lang.IllegalArgumentException()
        }

    }

    //Calculate the perimeter of circle with the diameter
    fun circlePerimeter(diameter: Int): Double {
        return PI*diameter
    }

    //Calculate the perimeter of square with a side
    fun squarePerimeter(side: Double): Double {
        return side*4
    }

    //Calculate the volume of the sphere with the radius
    fun sphereVolume(radius: Double): Double {
        return (4*PI*radius*radius*radius)/3
    }

    //Calculate the area of regular pentagon with one side
    fun pentagonArea(side: Int): Float {
        var y: Float = 1.72F
        val resultado:Float= side.toFloat() * side.toFloat() * y
        val solution: Double = Math.round(resultado * 10.0) / 10.0
        return solution.toFloat()
    }

    //Calculate the Hypotenuse with two legs
    fun calculateHypotenuse(legA: Double, legB: Double): Double {
        return hypot(legA,legB)
    }

}