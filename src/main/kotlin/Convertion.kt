import java.lang.IllegalArgumentException

/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
class Convertion {
    //convert of units of Metric System
    //Km to metters
    fun kmToM1(km: Double): Int {
        return (km * 1000).toInt()
    }

    //Km to metters
    fun kmTom(km: Double): Double {
        return (km * 1000)
    }

    //Km to cm
    fun kmTocm(km: Double): Double {
        return (km * 100000)
    }

    //milimetters to metters
    fun mmTom(mm: Int): Double {
        return mm.toDouble()/1000
    }

    //convert of units of U.S Standard System
    //convert miles to foot
    fun milesToFoot(miles: Double): Double {
        return miles*5280
    }

    //convert yards to inches
    fun yardToInch(yard: Int): Int {
        return yard*3
    }

    //convert inches to miles
    fun inchToMiles(inch: Double): Double {
        return inch/63360
    }

    //convert foot to yards
    fun footToYard(foot: Int): Int {
        return foot/3
    }

    //Convert units in both systems
    //convert Km to inches
    fun kmToInch(km: String?): Double {

        if(km?.toDoubleOrNull()!! in(-9999999.9..999999.9)){
        val x = km?.toDoubleOrNull()
        return x!!*39370.078
        }else {
            throw Exception("Error")
        }

    }

    //convert milimmeters to foots
    fun mmToFoot(mm: String?): Double {
        val x = mm?.toDoubleOrNull()
        return x!!*0.00328083919
    }

    //convert yards to cm
    fun yardToCm(yard: String?): Double {
        val x = yard?.toDoubleOrNull()
        return x!! * 91.44
    }
}