import org.junit.Assert.*
import org.junit.Test
import kotlin.test.assertFailsWith

class GeometricTest {

    private val geometric = Geometric()
    @Test
    fun testSquareArea1() {
        assertSame(16, geometric.squareArea(side = 4))
    }

    @Test
    fun testSquareArea2() {
        assertSame(15, geometric.squareArea(sideA = 3, sideB = 5))
    }

    @Test
    fun testSquareArea3()
    {
        assertSame(16, geometric.squareArea(4,4))
    }

    @Test
    fun testSquareArea4()
    {
        assertFailsWith<IllegalArgumentException> {geometric.squareArea(-2, -2)}
    }

    @Test
    fun testSquareArea5()
    {
        //assertEquals(IllegalArgumentException::class.java, geometric.squareArea(side = -2))-> Se comentarea y no se ajusta con el fin de dejar la comparativa entre los dos test
    }

    @Test
    fun testCircleArea1() {
      // assertEquals(/* expected = */ 28.27, /* actual = */ geometric.circleArea(radius = 0.0),0.01) - se envia un radio 0 por lo que su valor resultante no va a ser diferente de 0

    }
    @Test
    fun testCircleArea2() {
        assertEquals(0.0, geometric.circleArea(0.0), 0.01)
    }
    @Test
    fun testCircleArea3() {
        assertFailsWith<IllegalArgumentException> {geometric.circleArea(-3.0)}
    }

    @Test
    fun testCirclePerimeter() {
        assertEquals(21.99, geometric.circlePerimeter(7), 0.01)
    }

    @Test
    fun testSquarePerimeter() {
        // assertEquals(81.0, geometric.squarePerimeter(9.0), 0.0) -> Se comentarea ya que el resultado esperado es el area y no el perimetro
    }
    @Test
    fun testSphereVolume1() {
        assertEquals(14137.1, geometric.sphereVolume(15.0), 1.0)
    }
    @Test
    fun testSphereVolume2() {
        assertEquals(4188.79, geometric.sphereVolume(10.0), 1.0)
    }
    @Test
    fun testPentagonArea() {
        assertEquals(84.3f, geometric.pentagonArea(side = 7))
    }

    @Test
    fun testCalculateHypotenuse() {
        assertEquals(5.0, geometric.calculateHypotenuse(4.0, 3.0), 0.0)

        assertEquals(5.0, geometric.calculateHypotenuse(3.0, 4.0), 0.0)
    }
}